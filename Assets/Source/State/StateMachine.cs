﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine {
    /// <summary>
    /// The current state.
    /// </summary>
    public IState CurrentState { get; private set; }

    /// <summary>
    /// Initiates exit code of old state and then initiates start code of new state, in that order.
    /// </summary>
    /// <param name="newState"></param>
    public void ChangeState(IState newState)
    {
        if (CurrentState != null) CurrentState.Exit();
        CurrentState = newState;
        CurrentState.Enter();
    }

    /// <summary>
    /// Initiates execute code of the current state.
    /// </summary>
    public void Update()
    {
        if (CurrentState != null) CurrentState.Update();
    }

    public void FixedUpdate()
    {
        if (CurrentState != null) CurrentState.FixedUpdate();
    }
}
