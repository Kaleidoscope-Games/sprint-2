﻿using UnityEngine;

namespace State.Action
{
    public class DropState : IState
    {
        private Player player;

        public DropState(Player player)
        {
            this.player = player;
        }

        public void Enter()
        {
            player.Dump();
        }

        public void Update()
        {
            //Idle
            player.actionSM.ChangeState(player.ActionIdle);
        }

        public void FixedUpdate()
        {
        }

        public void Exit()
        {
        }
    }
}

