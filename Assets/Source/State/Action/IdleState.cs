﻿using UnityEngine;

namespace State.Action
{
    public class IdleState : IState
    {
        private Player player;

        public IdleState(Player player)
        {
            this.player = player;
        }

        public void Enter()
        {
        }

        public void Update()
        {
            //Action
            if (player.AimAxis >= -Constants.ANALOG_TOLERANCE && player.ShootButton)
            {
                player.actionSM.ChangeState(player.Action);
            }
            //Toss
            if (player.AimAxis < -Constants.ANALOG_TOLERANCE && player.ShootButton)
            {
                player.actionSM.ChangeState(player.Drop);
            }
        }

        public void FixedUpdate()
        {
        }

        public void Exit()
        {
        }
    }
}

