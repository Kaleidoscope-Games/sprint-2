﻿using UnityEngine;

namespace State.Action
{
    public class FireState : IState
    {
        private Player player;

        public FireState(Player player)
        {
            this.player = player;
        }

        public void Enter()
        {
            player.Throw();
        }

        public void Update()
        {
            //Idle
            player.actionSM.ChangeState(player.ActionIdle);
        }

        public void FixedUpdate()
        {
        }

        public void Exit()
        {
        }
    }
}