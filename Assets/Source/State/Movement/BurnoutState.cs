﻿using UnityEngine;

namespace State.Movement
{
	public class BurnoutState : IState
	{
		private Player player;
	    private float startTime;

		public BurnoutState(Player player)
		{
			this.player = player;
		}

		public void Enter()
		{
		    player.animatorSelf.SetBool("Burnout", true);
            startTime = Time.time;
		}

		public void Update()
		{
            //Idle
		    if (Time.time - startTime >= player.minBurnoutTime &&
		        player.ReverseAxis < Constants.TRIGGER_TOLERANCE)
		    {
		        player.movementSM.ChangeState(player.Boost);
		    }
			//Boost
			else if (player.ReverseAxis < Constants.TRIGGER_TOLERANCE ||
			         player.MoveAxis < Constants.TRIGGER_TOLERANCE)
		    {
		        player.movementSM.ChangeState(player.MoveIdle);
		    }
        }

		public void FixedUpdate()
		{
			
		}

		public void Exit()
		{
		    player.animatorSelf.SetBool("Burnout", true);
        }
	}
}