﻿using UnityEngine;

namespace State.Movement
{
    public class BrakeState : IState
    {
        private Player player;

        public BrakeState(Player player)
        {
            this.player = player;
        }
        
        public void Enter()
        {
            player.animatorSelf.SetBool("Brake", true);
        }

        public void Update()
        {
            //Move
            if (player.ReverseAxis < Constants.TRIGGER_TOLERANCE &&
                player.CurrentSpeed > Constants.IDLE_TOLERANCE)
            {
                player.movementSM.ChangeState(player.Move);
            }
            //Idle
            else if (player.CurrentSpeed < Constants.IDLE_TOLERANCE)
            {
                player.movementSM.ChangeState(player.MoveIdle);
            }
        }

        public void FixedUpdate()
        {
            player.Rotate(player.TurnAxis, true);
            player.Stop(player.ReverseAxis);
        }

        public void Exit()
        {
            player.animatorSelf.SetBool("Brake", true);
        }
    }
}