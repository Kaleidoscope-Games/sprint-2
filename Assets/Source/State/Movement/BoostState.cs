﻿using UnityEngine;

namespace State.Movement
{
    //Todo: implement more robust boost
    //using the boost curve as a scale-off for speed rather than a sudden stop
    public class BoostState : IState
    {
        private Player player;
        private float startTime;

        public BoostState(Player player)
        {
            this.player = player;
        }
        public void Enter()
        {
            player.soundObject.boost.Play();
            player.animatorSelf.SetBool("Boost", true);
            startTime = Time.time;
        }

        public void Update()
        {
            //Move
            if (Time.time - startTime >= player.boostTime) //change later when made more robust
            {
                player.movementSM.ChangeState(player.Move);
            }
        }

        public void FixedUpdate()
        {
            player.Rotate(player.TurnAxis, true);
            player.Burst();
        }

        public void Exit()
        {
            player.soundObject.boost.Stop();
            player.animatorSelf.SetBool("Boost", false);
        }
    }
}