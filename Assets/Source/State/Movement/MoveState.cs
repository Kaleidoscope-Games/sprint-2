﻿using System;
using UnityEngine;

namespace State.Movement
{
    public class MoveState : IState
    {
        private Player player;

        public MoveState(Player player)
        {
            this.player = player;
        }

        public void Enter()
        {
            player.animatorSelf.SetBool("Move", true);
            player.soundObject.move.Play();
        }

        public void Update()
        {
            //Drifting
            if (player.DriftButton && Mathf.Abs(player.TurnAxis) > Constants.ANALOG_TOLERANCE)
            {
                player.movementSM.ChangeState(player.Drift);
            }
            //Idle
            else if (player.CurrentSpeed < Constants.IDLE_TOLERANCE)
            {
                player.movementSM.ChangeState(player.MoveIdle);
            }
            //Braking
            else if (player.ReverseAxis > Constants.TRIGGER_TOLERANCE || //if both acceleration and brake is held down, brake takes priority
                     player.ReverseAxis > Constants.TRIGGER_TOLERANCE && 
                     player.MoveAxis > Constants.TRIGGER_TOLERANCE)
            {
                player.movementSM.ChangeState(player.Brake);
            }
        }

        public void FixedUpdate()
        {
            player.Rotate(player.TurnAxis, true);
            player.Accelerate(player.MoveAxis);
        }

        public void Exit()
        {
            player.animatorSelf.SetBool("Move", false);
            player.soundObject.move.Stop();
        }
    }
}