﻿using UnityEngine;

namespace State.Movement
{
    //Todo: make more robust
    public class DriftState : IState
    {
        private Player player;
        private float velocity;
        private float startTime;
        private bool right;

        public DriftState(Player player)
        {
            this.player = player;
        }
        public void Enter()
        {
            player.soundObject.drift.Play();
            player.animatorSelf.SetBool("Drift", true);
            velocity = player.CurrentSpeed;
            startTime = Time.time;
            right = player.TurnAxis > 0;
        }

        public void Update()
        {
            //boost
            if (Time.time - startTime >= player.driftTime && !player.DriftButton)
            {
                player.movementSM.ChangeState(player.Boost);
            }
            else if (!player.DriftButton)
            {
                player.movementSM.ChangeState(player.Move);
            }
        }

        public void FixedUpdate()
        {
            player.Slide(velocity, player.TurnAxis, right); //make it able to slow down rather than a fixed constant speed always
        }

        public void Exit()
        {
            player.soundObject.drift.Stop();
            player.animatorSelf.SetBool("Drift", false);
        }
    }
}