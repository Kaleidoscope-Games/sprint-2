﻿using System;
using UnityEngine;
using UnityEngine.Experimental.Input;

namespace State.Movement
{
    public class IdleState : IState
    {
        private Player player;

        public IdleState(Player player)
        {
            this.player = player;
        }

        public void Enter()
        {
            player.animatorSelf.SetBool("Idle", true);
        }

        public void Update()
        {
            //Burnout
            if (player.ReverseAxis > Constants.TRIGGER_TOLERANCE &&
                     player.MoveAxis > Constants.TRIGGER_TOLERANCE)
            {
                player.movementSM.ChangeState(player.Burnout);
            }
            //Moving
            else if(player.MoveAxis > Constants.TRIGGER_TOLERANCE ||
                player.CurrentSpeed > Constants.IDLE_TOLERANCE &&
                player.ReverseAxis < Constants.TRIGGER_TOLERANCE)
            {
                player.movementSM.ChangeState(player.Move);
            }
            //Reversing
            else if (player.ReverseAxis > Constants.TRIGGER_TOLERANCE)
            {
                player.movementSM.ChangeState(player.Reverse);
            }
        }

        public void FixedUpdate()
        {
            player.Rotate(player.TurnAxis, true);
        }

        public void Exit()
        {
            player.animatorSelf.SetBool("Idle", false);
        }
    }
}
