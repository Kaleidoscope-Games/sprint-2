﻿using UnityEngine;

namespace State.Movement
{
    public class ReverseState : IState
    {
        private Player player;

        public ReverseState(Player player)
        {
            this.player = player;
        }
        public void Enter()
        {
            player.animatorSelf.SetBool("Reverse", true);
            player.PlayerCameraBehavior.ToggleReverse(); //get rid of it
        }

        public void Update()
        {
            //Move
            if (player.MoveAxis > Constants.TRIGGER_TOLERANCE)
            {
                player.movementSM.ChangeState(player.Move);
            }
            //Idle
            else if (player.CurrentSpeed < Constants.IDLE_TOLERANCE)
            {
                player.movementSM.ChangeState(player.MoveIdle);
            }

            
        }

        public void FixedUpdate()
        {
            player.Rotate(player.TurnAxis, false);
            player.NegativeAccelerate(player.ReverseAxis);
        }

        public void Exit()
        {
            player.animatorSelf.SetBool("Reverse", true); 
            player.PlayerCameraBehavior.ToggleReverse();//this shit: get rid of it
        }
    }
}