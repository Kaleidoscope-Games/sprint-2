﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this script does nothing and is attached to the Cube in ExampleScene
//it's purpose is to demonstrate some basic fundamentals in coding within Unity

//The colon exists because that is called subclassing
//Most, if not all Unity scripts will subclass MonoBehavior, so don't fiddle with it unless you know why you're doing it
public class ExampleScript : MonoBehaviour
{
	private GameObject cube;
	private Camera mainCamera;
	private bool someImportantBoolean;
	
	//Awake is typically used to take care of actions before the scene's objects are loaded
	//These are things such as finding object tags, ensuring that a certain object is present, etc
	//Awake happens before Start() and only once
	private void Awake()
	{
		mainCamera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
		//The above does two things:
		//First, it finds using a function found in the GameObject CLASS the first GameObject tagged with "MainCamera"
		//Secondly, it gets the component found inside that object called "Camera" (if it doesn't find it, it will crash)
		//The second part is there because we can't assign incompatable classes. I defined "mainCamera" with the Camera class
		//The FindWithTag function returns a GameObject class, but I need the Camera class, which I KNOW exists (because
		//there's a camera in every scene).
	}
	
	//Start is used to start up the things after everything is (assumed) to be loaded on the screen
	//Start happens before the first Update() call and only once
	private void Start ()
	{
		//this assigns the reserved "cube" variable of type GameObject to the game object that this script is attached to
		cube = this.gameObject; //note that this is a gameObject PROPERTY not the GameObject CLASS
		//the above only works because the script is attached to the object
		//if you want to find a different object outside of the attached object that this script is on, use a Find function
	}
	
	//Update is called once per frame and is used to handle most game logic and ESPECIALLY player-sensitive logic, such
	//as gathering key input. Putting something in update is the only way to ensure that something like key input will
	//always be recognized by the game. Don't put physics calculations here.
	private void Update () 
	{
		//this is where you would manipulate the game object
		//but I'm leaving it up to you how you want to do this
	}
	
	//FixedUpdate is used for time-sensitive calculations, such as physics calls
	//FixedUpdate is called a consistent amount of times compared to Update
	//
	//For example, Update may be called anywhere between 0-1000 frames-per-second, but FixedUpdate will always be called
	//160 times per second (these aren't real numbers, but they provide an example of their differences)
	//
	//This is also a very bastardized attempt at explaining frames, but it can be a hard topic to understand
	//Especially when all you really need to know is that you just need to put physics calls here
	//See this video for more: https://www.youtube.com/watch?v=ZukcUv3pyXQ
	private void FixedUpdate()
	{
		
	}
}
