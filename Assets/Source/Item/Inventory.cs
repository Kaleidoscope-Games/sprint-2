﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Todo: refactor all of it. Burn it all away.
public class Inventory : MonoBehaviour {
    public Stack<GameObject> playerInventory;
    public float pickUpRadius = 1.5F;
    public float revolutionDistance = 10F;
    //public LayerMask exclude;

    private Player player;
    private SphereCollider triggerCollider;

    private void Awake()
    {
        player = GetComponentInParent<Player>();
        triggerCollider = GetComponent<SphereCollider>();
        playerInventory = new Stack<GameObject>(10);
    }

    private void Start()
    {
        triggerCollider.isTrigger = true;
        triggerCollider.radius = pickUpRadius;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Constants.ITEM_TAG))
        {
            if (!other.gameObject.GetComponent<Item>().Taken)
            {
                PushInventory(other.gameObject);

                if (player.debug) Debug.Log("Picked up: " + other.gameObject.name);
            }
        }
    }

    private void FixedUpdate()
    {

    }

    public void PushInventory(GameObject obj)
    {
        var item = obj.GetComponent<Item>();
        playerInventory.Push(obj);
        player.maxSpeed -= item.weight;
        item.Taken = true;
        obj.GetComponent<Collider>().enabled = false;
        obj.GetComponent<Rigidbody>().isKinematic = true;
        obj.transform.parent = this.transform;
        obj.transform.localPosition = Vector3.forward;
    }

    public GameObject PopInventory()
    {
        var obj = playerInventory.Pop();
        player.maxSpeed += obj.GetComponent<Item>().weight;
        StartCoroutine(Player.Cooldown(() => obj.GetComponent<Item>().Taken = false, 1F));
        obj.GetComponent<Collider>().enabled = true;
        obj.GetComponent<Rigidbody>().isKinematic = false;
        obj.transform.parent = null;
        return obj;
    }

    //public void SetPosition(GameObject obj)
    //{
    //    var rb = obj.GetComponent<Rigidbody>();
    //    rb.position = -player.transform.forward * -revolutionDistance;
    //}
}
