﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Todo: refactor all of it.
public class Item : MonoBehaviour
{
    public bool Taken { get; set; }
    public Player player;

    private Rigidbody rb;
    private bool bounce = false;

    public float weight = 10F;
    public float springForce = 300F;
    public float springHeight = 10F;
    public AnimationCurve bobBehavior = AnimationCurve.Linear(0F, 0F, 1F, 30F);
    private RaycastHit groundHit;
    private float count = 0;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        Taken = false;
    }

    private void Update()
    {
    }

    //should do state machine for these too
    private void FixedUpdate()
    {
        //oh my god fix this
        count += 0.01F;
        if (count == 1) count = 0;

        if (!Taken)
        {
            Ray ray = new Ray(transform.position, -transform.up);
            if (Physics.Raycast(ray, out groundHit, springHeight))
            {
                float proportionalHeight = (springHeight - groundHit.distance) / springHeight;
                Vector3 appliedHoverForce = Vector3.up * proportionalHeight * springForce;
                rb.AddForce(appliedHoverForce, ForceMode.Acceleration);
            }
        }
        else Revolve();
    }

    public void Revolve()
    {
        if (transform.parent != null) //error log galore without it on throws
        {
            transform.RotateAround(transform.parent.position, Vector3.up, 80 * Time.deltaTime);
//            transform.position = new Vector3(
//                transform.position.x,
//                10 * bobBehavior.Evaluate(count),
//                transform.position.z);
        }
    }

    //public void Revolve(Player player)
    //{
    //    Vector3 relative = transform.position - player.transform.position;
    //    Vector3 normalized = new Vector3(relative.x, 0, relative.z).normalized;
    //    Vector3 direction = Vector3.Cross(Vector3.up, normalized);

    //    rb.velocity = direction * 40F;
    //}

    /// <summary>
    /// This calculates the position from center (0,0) when given a degree rotation.
    /// </summary>
    /// <param name="x"> current x position </param>
    /// <param name="z"> current z position </param>
    /// <param name="rotation"> angle of pivot </param>
    /// <remarks>
    /// https://math.stackexchange.com/questions/103202/calculating-the-position-of-a-point-along-an-arc
    /// </remarks>
    private Vector3 CalculatePivotedPosition(float x, float z, float rotation)
    {
        var i = x * Mathf.Cos(rotation) - z * Mathf.Sin(rotation);
        var j = z * Mathf.Cos(rotation) + x * Mathf.Sin(rotation);
        return new Vector3(i, transform.localPosition.y, j);
    }
}
