﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteBehavior : MonoBehaviour
{
    public bool lockVerticalRotation = false;
    private GameObject cameraToFollow;

    private void Start()
    {
        if (this.CompareTag("Player1")) cameraToFollow = GameMonitor._instance.MainCamera;
        else if (this.CompareTag("Player2")) cameraToFollow = GameMonitor._instance.SecondaryCamera;
        else Debug.LogError(this.gameObject.name + " does not have an appropriate tag!");
    }

    private void LateUpdate()
    {
        transform.LookAt(cameraToFollow.transform);
        if (lockVerticalRotation)
            this.transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
    }
}
