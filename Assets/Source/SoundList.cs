﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundList : MonoBehaviour
{

	public SoundPlayer move;
	public SoundPlayer drift;
	public SoundPlayer boost;

	private void Start()
	{
		move = transform.GetChild(0).GetComponent<SoundPlayer>();
		drift = transform.GetChild(1).GetComponent<SoundPlayer>();
		boost = transform.GetChild(2).GetComponent<SoundPlayer>();
	}
}
