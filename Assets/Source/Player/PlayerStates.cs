﻿public partial class Player
{
    public IState MoveIdle;
    public IState Move;
    public IState Brake;
    public IState Burnout;
    public IState Reverse;
    public IState Drift;
    public IState Boost;

    public IState ActionIdle;
    public IState Action;
    public IState Drop;

    private void CreateStates()
    {
        MoveIdle = new State.Movement.IdleState(this);
        Move = new State.Movement.MoveState(this);
        Brake = new State.Movement.BrakeState(this);
        Burnout = new State.Movement.BurnoutState(this);
        Reverse = new State.Movement.ReverseState(this);
        Drift = new State.Movement.DriftState(this);
        Boost = new State.Movement.BoostState(this);

        ActionIdle = new State.Action.IdleState(this);
        Action = new State.Action.FireState(this);
        Drop = new State.Action.DropState(this);
    }
}
