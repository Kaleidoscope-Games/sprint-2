﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Todo: decouple from statemachine
public class CameraBehavior : MonoBehaviour
{
    public float smoothing = 0.5F;
    public float boostZoomSpeed = 2F;
    public float boostFOVValue = 50F;
    public AnimationCurve zoomCurve = AnimationCurve.Linear(0F, 27F, 80F, 42F); //0; 27(default FOV); 80 (maxSpeed);27 + 15 -- change to constants??
    public AnimationCurve rotationCurve = AnimationCurve.Linear(-1F, -6F, 1F, 6F); //cubic behavior
    public AnimationCurve rotationCurvePosition = AnimationCurve.Linear(-1F, 0F, 1F, 6F); //same as above behavior

    private Player player;
    private Camera affectedCamera;
    private bool pause = false;
    private bool boost = false;
    private bool boosted = false;
    private bool reverse = false;
    private float originalFOV;
    private float previousFOV;
    private float maxDeviance;
    private Transform originalTransform;
    
    private void Awake()
    {
        player = transform.parent.gameObject.GetComponent<Player>();
        affectedCamera = GetComponent<Camera>();
        originalFOV = affectedCamera.fieldOfView;
        originalTransform = affectedCamera.transform;
    }

    private void Start()
    {
        //maxDeviance = CalculatePivotedPosition(
        //    transform.localPosition.x, transform.localPosition.z,
        //    Mathf.Abs(rotationCurve.keys[0].value)).x;
        //Debug.Log(maxDeviance);
    }

    private void LateUpdate()
    {
        if (pause) return;
        if (!boost)
        {
            //zoom
            affectedCamera.fieldOfView = zoomCurve.Evaluate(player.CurrentSpeed);
            if (!reverse)
            {
                //rotation
                var rotation = rotationCurve.Evaluate(player.TurnAxis);
                transform.localEulerAngles = new Vector3(
                    transform.localEulerAngles.x,
                    rotation,
                    transform.localEulerAngles.z);
                //position
                transform.localPosition = 
                    new Vector3( rotationCurvePosition.Evaluate(player.TurnAxis), transform.localPosition.y, transform.localPosition.z);
            }
        }
        else BoostZoom();
    }

    public void ToggleZoom()
    {
        pause = !pause;
    }

    /// <summary>
    /// Called by state.
    /// </summary>
    public void ToggleBoostZoom()
    {
        boost = !boost;
        boosted = false;
    }

    /// <summary>
    /// Called by state.
    /// </summary>
    public void ToggleReverse()
    {
        reverse = !reverse;
        FlipCamera();
    }

    private void BoostZoom()
    {
        if (!boosted)
        {
            previousFOV = affectedCamera.fieldOfView;
            affectedCamera.fieldOfView = Mathf.Lerp(originalFOV, boostFOVValue, boostZoomSpeed);
            boosted = true;
        }
    }

    private void FlipCamera()
    {
        affectedCamera.transform.localEulerAngles = new Vector3(
            originalTransform.localEulerAngles.x,
            originalTransform.localEulerAngles.y + 180F,
            originalTransform.localEulerAngles.z);
        affectedCamera.transform.localPosition = new Vector3(
            originalTransform.localPosition.x,
            originalTransform.localPosition.y,
            originalTransform.localPosition.z * -1);
    }
}
