﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Experimental.Input;

//Todo: split into more components, comment
public partial class Player : MonoBehaviour
{
    #region State Machines
    /// <summary>
    /// State machine for movement.
    /// </summary>
    [HideInInspector] public StateMachine movementSM = new StateMachine();
    /// <summary>
    /// State machine for actions.
    /// </summary>
    [HideInInspector] public StateMachine actionSM = new StateMachine();
    #endregion
    #region Player Variables
    public bool debug = false;
    public int playerID = 0;
    public float turnSpeed = 2F;
    public float maxSpeed = 80F;
    public float maxReverseSpeed = 30F;
    public float springForce = 300F;
    public float springHeight = 10F;
    public float minBurnoutTime = 2F;
    public float boostAmount = 30F;
    public float boostTime = 1.5F;
    public float driftMagnitude = 0.25F;
    public float driftTime = 2F;
    public float throwMagnitude = 100F;
    public float throwAngle = 45F;
    public LayerMask whatIsGround;
    public AnimationCurve accelerationCurve = AnimationCurve.Linear(0F, 0F, 1F, 30F);
    public AnimationCurve reverseCurve = AnimationCurve.Linear(0F, 0F, 1F, 30F);
    public AnimationCurve brakeCurve = AnimationCurve.Linear(0F, 0F, 1F, 30F);
    public AnimationCurve boostCurve = AnimationCurve.Linear(0F, 0F, 1F, 30F);

    public Animator animatorSelf;
    public Animator animatorOther;
    private RaycastHit groundHit;
    private Rigidbody rb;
    private Gamepad gamepad;
    private bool firstRun = true;
    public float CurrentSpeed { get; private set; }
    public CameraBehavior PlayerCameraBehavior { get; private set; } //only used for state machines to update camera behavior; find better solution
    private Inventory inventory; //Todo: refactor how items work
    public SoundList soundObject; //Temporary solution
    #endregion
    #region Input Information
    /// <summary>
    /// L-Stick X-Axis.
    /// </summary>
    [HideInInspector] public float TurnAxis { get; private set; }
    /// <summary>
    /// R-Trigger. 
    ///     /// </summary>
    [HideInInspector] public float MoveAxis { get; private set; }
    /// <summary>
    /// L-Trigger;
    ///</summary>
    [HideInInspector] public float ReverseAxis { get; private set; }
    /// <summary>
    /// L/R-Shoulder.
    /// </summary>
    [HideInInspector] public bool DriftButton { get; private set; }
    /// <summary>
    /// East Button.
    /// </summary>
    [HideInInspector] public bool ShootButton { get; private set; }
    /// <summary>
    /// North Button.
    /// </summary>
    [HideInInspector] public bool TossButton { get; private set; }
    /// <summary>
    /// L-Stick Y-Axis.
    /// </summary>
    [HideInInspector] public float AimAxis { get; private set; }
    #endregion


    private void Awake()
    {
        CreateStates();

        rb = gameObject.GetComponent<Rigidbody>();
        PlayerCameraBehavior = GetComponentInChildren<CameraBehavior>();
        inventory = GetComponentInChildren<Inventory>(); //Todo: refactor how items work
        animatorSelf = transform.GetChild(1).GetComponent<Animator>();
        animatorOther = transform.GetChild(2).GetComponent<Animator>();
        soundObject = transform.GetChild(4).GetComponent<SoundList>(); //temporary solution
    }

    private void Start()
    {
        //Entry states
        movementSM.ChangeState(Reverse);
        actionSM.ChangeState(ActionIdle);
    }

    private void Update()
    {
        if (debug)
        {
            var thing = transform.rotation * new Vector3(0,1,1).normalized * 50;
            Debug.DrawRay(transform.position,thing, Color.blue);
        }

        if (firstRun)
        {
            gamepad = Gamepad.all[playerID];
            firstRun = false;
        }


        TurnAxis = gamepad.leftStick.ReadValue().x;
        AimAxis = gamepad.leftStick.ReadValue().y;
        MoveAxis = gamepad.rightTrigger.ReadValue();
        ReverseAxis = gamepad.leftTrigger.ReadValue();
        DriftButton = gamepad.rightShoulder.ReadValue() > 0 || gamepad.leftShoulder.ReadValue() > 0;
        ShootButton = gamepad.buttonEast.ReadValue() > 0;
        TossButton = gamepad.buttonNorth.ReadValue() > 0;

        CurrentSpeed = Get2DMagnitude(rb.velocity.x, rb.velocity.z);

        animatorSelf.SetFloat("Turning", TurnAxis);

        movementSM.Update();
        actionSM.Update();
    }

    private void FixedUpdate()
    {
        movementSM.FixedUpdate();
        actionSM.FixedUpdate();

        Ray ray = new Ray(transform.position, -transform.up);
        if (Physics.Raycast(ray, out groundHit, springHeight))
        {
            float proportionalHeight = (springHeight - groundHit.distance) / springHeight;
            Vector3 appliedHoverForce = Vector3.up * proportionalHeight * springForce;
            rb.AddForce(appliedHoverForce, ForceMode.Acceleration);
        }
    }

    //Todo: cleanup
    public void Rotate(float rotation, bool forward)
    {
        Quaternion deltaRotation = Quaternion.Euler(rotation * new Vector3(0F, turnSpeed, 0F));
        float mag = Get2DMagnitude(rb.velocity.x, rb.velocity.z);
        rb.MoveRotation(rb.rotation * deltaRotation);
        if (forward)
        {
            //apply velocity in the direction the player is facing
            rb.velocity = new Vector3(transform.forward.x * mag, rb.velocity.y, transform.forward.z * mag);
        }
        else
        {
            //apply velocity in the direction the player is facing
            rb.velocity = new Vector3(-transform.forward.x * mag, rb.velocity.y, -transform.forward.z * mag);
        }
    }

    //Todo: unjank + finish the code
    //notes: 
    //do not allow players to adjust drift too much
    //the deltarotation is awkwardly specific -- find algorithm that will give us that arbitrary number
    public void Slide(float amount, float rotation, bool right)
    {
        float sign = right ? 1F : -1F;
        Quaternion deltaRotation = Quaternion.Euler((rotation / 1.25F + sign * driftMagnitude) * new Vector3(0F, turnSpeed, 0F));
        rb.MoveRotation(rb.rotation * deltaRotation);
        var diagonal = transform.rotation * new Vector3(-sign * 0.5F, 0F, 0.5F);
        rb.velocity = new Vector3(diagonal.x * amount * 2 , rb.velocity.y, diagonal.z * amount * 2);
    }

    public void Accelerate(float amount)
    {
        rb.velocity += transform.forward * accelerationCurve.Evaluate(amount);
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);
    }

    public void Burst()
    {
        rb.velocity = transform.forward * (maxSpeed + boostAmount);
    }

    public void NegativeAccelerate(float amount)
    {
        rb.velocity -= transform.forward * reverseCurve.Evaluate(amount);
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxReverseSpeed);
    }

    public void Stop(float amount)
    {
        rb.velocity -= transform.forward * brakeCurve.Evaluate(amount);
    }

    //Todo: refactor how items work
    public void Throw()
    {
        if (inventory.playerInventory.Count != 0)
        {
            Vector3 throwDirection = transform.rotation * new Vector3(0, 1, 1).normalized;
            GameObject item = inventory.PopInventory();
            item.GetComponent<Rigidbody>().velocity = rb.velocity;
            item.GetComponent<Rigidbody>().AddForce(throwDirection * throwMagnitude, ForceMode.VelocityChange);
        }
    }

    //Todo: refactor how items work
    public void Dump()
    {
        if (inventory.playerInventory.Count != 0)
        {
            Vector3 throwDirection = transform.rotation * new Vector3(0, 1, -1).normalized;
            GameObject item = inventory.PopInventory();
            item.GetComponent<Rigidbody>().velocity = rb.velocity;
            item.GetComponent<Rigidbody>().AddForce(throwDirection * throwMagnitude, ForceMode.VelocityChange);
        }
    }

    #region Helper functions
    //Todo: fix
    /// <summary>
    /// Find child object of a given parent with the matching tag and return the child.
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="tag"></param>
    /// <returns></returns>
    public GameObject FindChildObjectWithTag(GameObject parent, string tag)
    {
        GameObject child = new GameObject();
        for (int i = 0; i < parent.transform.childCount; i++)
        {
            child = parent.transform.GetChild(i).gameObject;
            if (child.tag == tag) break;
        }
        if (child.tag != tag) Debug.LogError("Child could not be found.");
        return child;
    }

    /// <summary>
    /// Pythagorean theorem.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="z"></param>
    /// <returns></returns>
    public float Get2DMagnitude(float x, float z)
    {
        return Mathf.Sqrt(x * x + z * z);
    }
    
    /// <summary>
    /// Todo: Move this shit else where
    /// </summary>
    /// <param name="condition"></param>
    /// <param name="waitTime"></param>
    /// <returns></returns>
    public static IEnumerator Cooldown(System.Action condition, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        condition();
    }
    
    #endregion

    #region Debugging
    //Todo: make less redundant
    /// <summary>
    /// Debug GUI
    /// </summary>
    private void OnGUI()
    {
        if (debug)
        {
            float width = 400F;
            float height = 20F;
            float y = 18F;
            float x = 4F;
            DrawDebugGUI(x, y, width, height, playerID, playerID * Screen.height/2);
        }
    }

    private void DrawDebugGUI(float x, float y, float width, float height, int player, float startPos)
    {
        GUI.Box(new Rect(0, startPos, width + 10, y * 4 + 25), "");
        GUI.Label(
            new Rect(x, y * 0 + startPos, width, height),
            "Device Name (ID): " + gamepad.displayName + " (" + gamepad.id + ")");
        GUI.TextField(
            new Rect(x, y * 1 + startPos, width / 2, height),
            "Left stick X: " + TurnAxis);
        GUI.TextField(
            new Rect(x + width / 2, y * 1 + startPos, width / 2, height),
            "Left stick Y: " + AimAxis);
        GUI.TextField(
            new Rect(x, y * 2 + startPos, width / 3, height),
            "Right trigger: " + MoveAxis);
        GUI.TextField(
            new Rect(x + width / 3, y * 2 + startPos, width / 3, height),
            "Left trigger: " + ReverseAxis);
        GUI.TextField(
            new Rect(x + width / 3 * 2, y * 2 + startPos, width / 3, height),
            "Bumpers: " + DriftButton);
        GUI.TextField(
            new Rect(x, y * 3 + startPos, width / 2, height), 
            "North button: " + ShootButton);
        GUI.TextField(
            new Rect(x + width/2, y * 3 + startPos, width / 2, height),
            "East button: " + TossButton);
        GUI.TextField(
            new Rect(x, y * 4 + startPos, width / 5, height),
            "Speed: " + Get2DMagnitude(rb.velocity.x, rb.velocity.z));
        GUI.TextField(
            new Rect(x + width / 5, y * 4 + startPos, width * 4 / 5, height),
            String.Format("Current Player {0} State: {1}", player + 1, movementSM.CurrentState));
    }
    #endregion
}
