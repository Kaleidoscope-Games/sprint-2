﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMonitor : MonoBehaviour
{
    public static GameMonitor _instance = null;
    public GameObject MainCamera { get; private set; }
    public GameObject SecondaryCamera { get; private set; }

    private void Awake()
    {
        if (_instance == null)
        {
            //instance specfic initalization,
            //stuff outside this check will be run by the other instances of GameManager before they are destoryed
            _instance = this;

            DontDestroyOnLoad(gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }

        MainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        Debug.Log(MainCamera.transform.parent.GetComponent<Player>().playerID);
        SecondaryCamera = GameObject.FindGameObjectWithTag("SecondaryCamera");
    }
}
