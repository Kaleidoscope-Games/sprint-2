﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundPlayer : MonoBehaviour
{

	//public AudioMixer masterMixer;
	public AudioSource source;
	public bool loop;
	public bool autoStart = false;

	private void Start()
	{
		source = GetComponent<AudioSource>();
		source.loop = loop;
		if (autoStart) source.Play();
		else source.Stop();
	}

	public void Stop()
	{
		source.Stop();
	}

	public void Play()
	{
		source.Play();
	}
}
