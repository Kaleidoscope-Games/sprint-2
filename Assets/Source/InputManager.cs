﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Not functional; don't use
public class InputManager
{
    public static bool unboundKeys = false; //all keys must be bound
    private static Dictionary<string, KeyCode> keyMapping;
    private static Dictionary<KeyCode, string> inverseKeyMapping; //for reverse look-up; using a long dictionary instead of loop for performance

    /// <summary>
    /// Actions the players can do in-game.
    /// </summary>
    private static readonly string[] keyMaps =
    {
        //Player 1
        "P1Drift",
        "P1Forward",
        "P1Backward",
        "P1Left",
        "P1Right",
        //Player 2
        /*"P2Drift",
        "P2Forward",
        "P2Backward",
        "P2Left",
        "P2Right",*/
    };

    /// <summary>
    /// Default keybinds to the above actions.
    /// <remarks>
    /// See for more: http://wiki.unity3d.com/index.php/Xbox360Controller#Windows
    /// </remarks>
    /// </summary>
    private static readonly KeyCode[] defaults =
    {
        //Player 1
        KeyCode.LeftShift,
        KeyCode.W,
        KeyCode.S,
        KeyCode.A,
        KeyCode.D,
        //Player 2
        //Until Unity gets better about making controller inputs open, this is basically impossible without creating your own virtual controller
    };

    /// <summary>
    /// Remember to actually create this during game startup.
    /// </summary>
    static InputManager()
    {
        InitializeDictionary();
    }

    /// <summary>
    /// Initializes the dictionary with default settings.
    /// </summary>
    private static void InitializeDictionary()
    {
        keyMapping = new Dictionary<string, KeyCode>();
        inverseKeyMapping = new Dictionary<KeyCode, string>();
        for (int i = 0; i < keyMaps.Length; ++i)
        {
            keyMapping.Add(keyMaps[i], defaults[i]);
            inverseKeyMapping.Add(defaults[i], keyMaps[i]);
        }
    }

    /// <summary>
    /// Binds a KeyCode to a key map of available actions in the game. This exists so that we can change input during runtime. (need to write code to dump the array to a text file and read from a text file)
    /// </summary>
    /// <param name="keyMap"></param>
    /// <param name="key"></param>
    public static void SetKeyMap(string keyMap, KeyCode key)
    {
        if (!keyMapping.ContainsKey(keyMap)) //if keymap does not exist
            throw new ArgumentException("Invalid KeyMap in SetKeyMap: " + keyMap);
        if (keyMapping.ContainsValue(key)) //if key already bound 
        {//unbind old keybinds
            keyMapping[inverseKeyMapping[key]] = KeyCode.None;
            inverseKeyMapping[key] = "";
            unboundKeys = true;
        }

        if (!inverseKeyMapping.ContainsKey(key)) //if new key does not exist in inverse mapping
            inverseKeyMapping.Add(key, keyMap);
        else //update 
            inverseKeyMapping[key] = keyMap;
        keyMapping[keyMap] = key;
    }

    /// <summary>
    /// Returns true during the frame the user presses down the key.
    /// </summary>
    /// <param name="keyMap"></param>
    /// <returns></returns>
    public static bool GetKeyDown(string keyMap)
    {
        return Input.GetKeyDown(keyMapping[keyMap]);
    }

    /// <summary>
    /// Returns true during the frame the user releases the key.
    /// </summary>
    /// <param name="keyMap"></param>
    /// <returns></returns>
    public static bool GetKeyUp(string keyMap)
    {
        return Input.GetKeyUp(keyMapping[keyMap]);
    }

    /// <summary>
    /// Returns true while the user holds down the key.
    /// </summary>
    /// <param name="keyMap"></param>
    /// <returns></returns>
    public static bool GetKey(string keyMap)
    {
        return Input.GetKey(keyMapping[keyMap]);
    }
}
