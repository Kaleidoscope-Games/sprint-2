﻿public static class Constants
{
    public const float ANALOG_TOLERANCE = 0.05F;
    public const float TRIGGER_TOLERANCE = 0.001F;
    public const float IDLE_TOLERANCE = 0.8F;
    public const float CURVE_MAX = 100F;
    public const float CURVE_MIN = 0F;
    public const string ITEM_TAG = "Item";
}
